## Framework used

SELENIUM with Java and TestNg

## Prerequisites

> Eclipse IDE and Java Compiler compliance level- "12"

> JDK Version 16.0

> Chrome driver 91.0.4472.101 (chromedriver.exe)

## Comments

After each function call there's a sleep of upto 2 seconds 

File hbtechnical.java is for Scenario 1

For Firefox testing- add firefoxdriver.exe and run import org.openqa.selenium.firefox.FirefoxDriver;