package harshitbatratechie;


import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.Select;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


import com.google.common.io.Files;

 
//Name=Harshit BATRA
//Technical Challenge QA
//*************************SCENARIO-1********************


public class hbtechnical
{
	
	WebDriver driver;// Global 
	
	 public void tshot(String outputfile) throws IOException
	    { 
	    	TakesScreenshot sc = ((TakesScreenshot)driver); //convert driver obj to TakeScreenshot
			
			File scfile = sc.getScreenshotAs(OutputType.FILE); //Call getScreenshotAs method to create a file
		
		File output = new File(outputfile); //move file to new destination
		
		Files.copy(scfile, output);
		
	    } 
	
    @BeforeSuite
		    
    public void Tc_openbrowser()  //Opening Chrome browser
		    {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HA20131171\\Seleniumsoftware\\chromedriver.exe");
		        
		        driver = new ChromeDriver();    
		        
		    }
		    
    @Test
    
    /*SCENARIO 1
     -------------*/
   
   
		public void TC1_openurl_StorePage()
	     {
    	
    	//**TC-1entering the URL-Open store page
		driver.get("https://react-shooping-cart.netlify.app/");
		    // Delay in execution upto 2 seconds
	    try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    	
	    
	   //**TC-2 clicking on the ADD TO CART- selection of first element on store page-"Buffalo - Striploin"
		driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")).click();
		    // Delay in execution upto 2 seconds
	    try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
		//**TC-3 /Go to or Click CART
		driver.findElement(By.xpath("//header/a[3]")).click();
		// Delay in execution upto 2 seconds
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     }
    
            
     @Test      
            
          public void TC4_Check_CART() throws IOException
    		{
    	 
    	 /*//**TC-4 Checking item exist in Cart- "Screenshot"
    		 **TC-5 Checking values of total items and total payment- Again same screenshot- TC3 Cart is already open and
                  total item and total payment is visible without any clicks or opening 
    		 **TC-6 Checking delete button-//Again same screenshot- TC3 Cart is already open and
                  Delete button is visible without any clicks or opening
    		*/
            
            	tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshotCART.png");
            	// Delay in execution upto 2 seconds
            	try {
        			TimeUnit.SECONDS.sleep(2);
        		} catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            }
     
     
     @Test
            public void TC7_Click_Clear_Button()
            
            {
    	 
    	 //Click Clear Button
            	driver.findElement(By.xpath("//button[contains(text(),'CLEAR')]")).click(); 
            	// Delay in execution upto 2 seconds
            	try {
        			TimeUnit.SECONDS.sleep(2);
        		} catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            }
     @Test
            public void TC8_Check_Cart_Clear() throws IOException
            {
    	 
    	 /*Checking CART Clear-//Again screenshot- as per TC7 Cart is already open and
                is empty */ 
            	
            	tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshot_clear.png");
            	
            	// Delay in execution 
                try {
             		TimeUnit.SECONDS.sleep(2);
             	} catch (InterruptedException e) {
             		// TODO Auto-generated catch block
             		e.printStackTrace();
             	}
            }
}
            
           
