package harshitbatratechie;


import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.Select;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


import com.google.common.io.Files;

 
//Name=Harshit BATRA
//Technical Challenge QA
//*************************SCENARIO-2****************
public class scenario2 
{
	
	WebDriver driver;// Global 
	
	 public void tshot(String outputfile) throws IOException
	    { 
	    	TakesScreenshot sc = ((TakesScreenshot)driver); //convert driver obj to TakeScreenshot
			
			File scfile = sc.getScreenshotAs(OutputType.FILE); //Call getScreenshotAs method to create a file
		
		File output = new File(outputfile); //move file to new destination
		
		Files.copy(scfile, output);
		
	    } 
	
    @BeforeSuite
		    
    public void Tc_openbrowser()
		    {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HA20131171\\Seleniumsoftware\\chromedriver.exe");
		        
		        driver = new ChromeDriver();    
		        
		    }
		    
    @Test
    
    /*SCENARIO 2
     -------------*/
   
   
    public void TC1_Go2_STORE()
    {//**TC-1 Go to or Click Store
    driver.get("https://react-shooping-cart.netlify.app/"); 
          // Delay in execution upto 2 seconds
    try {
		TimeUnit.SECONDS.sleep(2);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    //**TC-2 clicking on 1.ADD TO CART- selection of second element on store page-"Wine - Gato Negro Cabernet"
    driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[2]/div[2]/div[1]/button[1]")).click();	
          // Delay in execution 
    try {
		TimeUnit.SECONDS.sleep(2);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    //**T-C2 clicking on 2.ADD TO CART- selection of third element on store page-"Bacardi Breezer - Tropical"
    driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[2]/div[3]/div[1]/button[1]")).click();
         // Delay in execution 
    try {
		TimeUnit.SECONDS.sleep(2);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    
    //**TC-3 Go to or Click CART BUTTON
    driver.findElement(By.xpath("//header/a[3]")).click(); 
    	 // Delay in execution 
       try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
       
   //**TC-4 Increase quantity to 3-- run this 2 times: "Clicking on + BUTTON"
    driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/button[1]/*[1]")).click(); 
    	 // Delay in execution 
       try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
   //Increase to 2-- run once again and quantity will be 3: "Clicking on + BUTTON"
    driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/button[1]/*[1]")).click();
    	// Delay in execution 
      try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();}
    }
    
  @Test 

    public void TC5_TC6__TC7_Check_vale_of_TotalItems_TotalPayment_and_Reduce_Button_and_DELETE_Button() throws IOException
    {
	  //**TC-5 TC-6 TC-7
	  /*Checking values of total items and total payemnt- Again screenshot- TC4 Cart is already open and
    total item and total payment is visible without any clicks or opening..
    Also, as already in Cart-- Reduce button is also seen on the same page for ITEM 1 in the screenshot 
    Delete button also can be seen on same page or acreenshot for ITEM 2*/
    	
    
    tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshot_4.png");
    
         // Delay in execution 
    try {
  		TimeUnit.SECONDS.sleep(2);
  	} catch (InterruptedException e) {
  		// TODO Auto-generated catch block
  		e.printStackTrace();}
    	
    }
    
@Test

    public void TC8_DECREASE_Quantity_2() 
    {
	//**TC-8 Decrease quantity to 2 for 1st ITEM: "Clicking on '-' BUTTON"
    driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/button[2]/*[1]")).click(); 
    	 // Delay in execution 
       try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
   }
   
@Test   

    public void TC9_Check_vale_of_TotalItems_TotalPayment_Again() throws IOException
    {
	//**TC-9
	/*Checking values of total items and total payment- Again same screenshot- TC8 Cart is already open and
    total item and total payment is visible without any clicks or opening */
    	
    	tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshot_5.png");
    	
    	 // Delay in execution 
        try {
     		TimeUnit.SECONDS.sleep(2);
     	} catch (InterruptedException e) {
     		// TODO Auto-generated catch block
     		e.printStackTrace();
     	}
    	
   //**TC-10  Delete 2nd item: 	
    	
   driver.findElement(By.xpath("//body/div[@id='root']/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[4]/button[2]")).click(); 
    	 // Delay in execution 
       try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
     
    //**TC-11
    /*Checking-Again screenshot- TC10 Cart is already open and
        ITEM IS DELETED*/
        	
       tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshot_6.png");
       
       // Delay in execution 
       try {
    		TimeUnit.SECONDS.sleep(2);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
            
    //**TC-12 clicking on checkout button:
       
       driver.findElement(By.xpath("//button[contains(text(),'CHECKOUT')]")).click();
            	// Delay in execution 
             try {
            		TimeUnit.SECONDS.sleep(2);
            	} catch (InterruptedException e) {
            		// TODO Auto-generated catch block
            		e.printStackTrace();
            	}
            
     //**TC-13_TC-14_Check_CHECKOUT_SUCCESSFUL_n_CART_EMPTY() throws IOException
                 /*Checking-Again screenshot- TC12 Cart is already open and
                Message is displayed on the screen "Checkout Successfully"
                Also, Cart is seen on same page and written as well "Cart Empty"- Sreenshot added*/
                	
                	tshot("C:\\Users\\HA20131171\\Desktop\\SavingScreenshot_7.png");
                	 // Delay in execution 
                    try {
                 		TimeUnit.SECONDS.sleep(2);
                 	} catch (InterruptedException e) {
                 		// TODO Auto-generated catch block
                 		e.printStackTrace();
                 	}
    } 
}
